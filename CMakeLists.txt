cmake_minimum_required(VERSION 3.10)

# this will be the project name
project("Awesome Cpp Project" VERSION 0.1.0)

# setting c++ standard
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# setting the executable result
add_executable(app src/main.cpp)
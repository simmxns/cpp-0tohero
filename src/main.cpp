#include <iostream>
#include <map>
#include <string>
#include <vector>

using std::string, std::vector, std::map;

auto main(int, char **) -> int
{
    string name = "";
    vector<int> v{1, 2, 3, 4};
    map<string, int> m{
        {"CPU", 17},
        {"GPU", 50},
        {"RAM", 88}
    };

    v.push_back(25);

    for (int n : v) {
        std::cout << n << '\n';
    }

    for (auto &iter : m) {
        std::cout << "{ " << iter.first << " : " << iter.second << " }" << '\n';
    }

    std::cout << "Enter your name: ";
    std::cin >> name;

    std::cout << "Hi " << name << std::endl;

    return 0;
}

// #19 returning a moved local variable
#include <iostream>
#include <vector>

// a vector can be large object and u dont want to make a copy of it
std::vector<int> makeVector(const int n)
{
  std::vector<int> v{1, 2, 3, 4, 5};
  std::vector<int> w{1, 2};

  // in all cases the move is unnecessary
  // the compiler always knows that it can move a local variable
  // but in some cases this actively prevents return value optimization
  // so thats why this is one of the few rules where should just never do this
  if (n == 0)
    return std::move(v);
  else 
    return std::move(w);
}

// #20 thinking that move actually moves something

template<typename T>
constexpr std::remove_reference_t<T> &&
// move(T &&value) noexcept
// a more accurte name is someting like =>
castToRvalue(T &&value) noexcept
{
  return static_cast<std::remove_reference_t<T> &&>(value);
}

constexpr int&&
// static cast it to an rvalue reference and returns it 
// move(int &value) noexcept
// the exact same thing happens in the or value overload
// it just static casts to an rvalue and returns it
castToRvalue(int &value) noexcept
// move(int &&value) noexcept
{
  
  return static_cast<int &&>(value);
}
// #14 forgetting to mark the destructors virtual *in a class hierarchy
#include <iostream>

class BaseWithNonvirtualDestructor
{
  public:
    void foo()
    {
      std::cout << "do foo\n";
    }

    virtual ~BaseWithNonvirtualDestructor()
    {
      std::cout << "called base destructor\n";
    }
};

// if a derived class gets deleted through a pointer to this base class
// then the derived class destructor will not be called, only the base class desctructor will be called

class Derived : public BaseWithNonvirtualDestructor {
  public:
    ~Derived() override
    {
      std::cout << "called derived destructor\n";
    }

    void consumeBase(std::unique_ptr<BaseWithNonvirtualDestructor> p)
    {
      p->foo();
      // deletes p when done
    }
};
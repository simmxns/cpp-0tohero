// #3 using a index for loop when a range-based for loop expresses the intent
// better
#include <iostream>

void trainModel(const std::vector<int> &data, auto &model)
{
    // BAD
    // for (std::size_t i = 0; i < data.size(); ++i) {
    //   model.update(data[i])
    // }

    // GOOD
    for (const auto &x : data) {
        model.update(x);
    }
}
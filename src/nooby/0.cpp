// #1 dont using namespaces as a global level, just use the stuff inside the
// header that u need
#include <iostream>

// BAD
// using namespace std;

// GOOD
using std::string, std::cout, std::endl;

void usingNamespaceStd()
{
    string s{"Hi, Earth!"};
    cout << s << endl;
}

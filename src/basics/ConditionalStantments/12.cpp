/*
Hacer un menu que considere las siguientes opciones:
1. Cubo de un numero
2. Numero par o impar
3. Salir
*/

#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int numero, opc, cubo = 0;
    cout << "\tBienvenido usuario :)" << endl;
    cout << "1. Cubo de un numero" << endl;
    cout << "2. Numero par o impar" << endl;
    cout << "3. Salir" << endl;
    cout << "Opcion: ";
    cin >> opc;

    switch (opc) {
    case 1:
        cout << "Introduce un numero: ";
        cin >> numero;

        cubo = pow((numero), 3);
        cout << cubo;
        break;

    case 2:
        cout << "Introduce un numero: ";
        cin >> numero;

        if (numero == 0) {
            cout << "Cero";
        } else if (numero % 2 == 0) {
            cout << "Par";
        } else {
            cout << "Impar";
        }
        break;
    case 3:
        break;
    }
    return 0;
}

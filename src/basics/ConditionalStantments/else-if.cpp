
/*
-- SINTAXIS DE UN IF --

if(condicion){
        Instruccion 1
} else {
        Instruccion 2
}

*/

#include <iostream>
using namespace std;

int main()
{
    int numero, dato = 5;

    cout << "valor de nuemero: ";
    cin >> numero;

    if (numero != dato) { // si no es igual a dato ejecutar...
        cout << "El numero no es 5";
    } else {
        cout << "El numero si es igual a 5";
    }

    return 0;
}

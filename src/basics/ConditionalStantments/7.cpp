/*
Pedir al usuario que te digite una edad que este en el rango solicitado
de (18-25)
*/

#include <iostream>
using namespace std;

int main()
{
    int edad;

    cout << "Edad: ";
    cin >> edad;

    if (edad > 17 || edad < 26) {
        cout << "Ok";
    } else {
        cout << "Fuera de rango";
    }
}

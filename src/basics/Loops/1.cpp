/*
Realizar un programa que solicite al usuario un numero del 1 al 10
y que al digitarlo te muestre su tabla de multiplicar
correspondiente
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    int numero;

    do {
        cout << "Digitar un numero: ";
        cin >> numero; // Pide al usuario digitar un numero y lo guarda en la
                       // variable numero
    } while ((numero < 1) ||
             (numero > 10)); // Pero... el numero sera guardado siempre y cuando
                             // numero no sea menor a 1 ni mayor 10

    for (int i = 1; i <= 10;
         i++) { // i se inicializa en 1 finaliza en 10 y va en aumentando
        cout << numero << " * " << i << " = " << numero * i
             << endl; // se muesta el numero digitado el inicializador igualado
                      // al resultado que es tu numero por el inicializador
    }

    cout << "\n\n";
    system("pause");
    return 0;
}

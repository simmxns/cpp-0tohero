/*
Escriba un programa que calcula el valor de:
2^1+2^2+2^3+...+2^n
*/
#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int i, n, elev = 0, suma = 0;

    cout << "Valor de n: ";
    cin >> n;

    for (i = 1; i <= n; i++) {
        elev = pow(2, i);
        suma += elev;
    }

    cout << "\nSuma: " << suma;

    return 0;
}

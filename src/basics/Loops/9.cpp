/*
Escribe un programa que calcule el valor de: 1*2*3...*n (factorial)

5! = 5*4*3*2*1
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    int i, n, multi = 1;

    cout << "Valor de n: ";
    cin >> n;

    for (i = n; i >= 1; i--) {
        multi *= i;
    }

    cout << "\nMultiplicacion: " << multi;

    system("pause");
    return 0;
}

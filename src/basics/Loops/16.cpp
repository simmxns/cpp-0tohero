/*
Hacer un programa que al digitar un numero entero
me de como resultado su descomposicion en numeros primos

Ejemplo: 20=2*2*5
*/

#include <iostream>
using namespace std;

int main()
{
    int numero, i;

    cout << "Digitame un numero: ";
    cin >> numero;

    cout << "Descomposicion: ";
    for (i = 2; i <= numero; i++) {
        while (numero % i == 0) {
            numero /= i;
            cout << i << " ";
        }
    }

    return 0;
}

/*
Hacer un programa que calcule el resultado de siguien expresion
1-2+3-4+5-6...n
*/

#include <iostream>
using namespace std;

int main()
{
    int i, n, suma = 0, resta = 0, op = 0;

    cout << "Valor de n: ";
    cin >> n;

    for (i = 1; i <= n; i++) {
        if (i % 2 == 0) {
            suma += i;
        } else {
            resta -= i;
        }
    }

    op = suma + resta;

    if (op % 2 == 0) {
        cout << op;
    } else {
        cout << op * (-1);
    }

    return 0;
}

/*
Escriba un programa que calcule x^y donde tanto x como y
son enteros positivos, sin utilizar la funcion pow
*/

#include <iostream>
using namespace std;

int main()
{
    int x, y, elev = 1;

    cout << "\t x^y" << endl;
    cout << "Exponente (x): " << endl;
    cin >> x;
    cout << "Potencia (y): " << endl;
    cin >> y;

    for (int i = 1; i <= y; i++) { // el for terminara cuando y lo diga
        elev *= x; //"x" sera multiplicado por "elev" cada repeticion se
                   // multiplicara, pero esta vez "elev" valdra el resultado que
                   // de con "x" por cada repeticion
    }

    cout << elev << endl;

    return 0;
}

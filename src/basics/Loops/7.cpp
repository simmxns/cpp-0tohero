/*
Escribe un programa que calcule el valor de: 1+2+3+...+n
*/

#include <iostream>
using namespace std;

int main()
{
    int n, suma = 0;

    cout << "Digita el valor de n: ";
    cin >> n;

    for (int i = 1; i <= n; i++) {
        suma += i;
    }

    cout << "\nSuma de todos los numeros: " << suma << endl;

    return 0;
}

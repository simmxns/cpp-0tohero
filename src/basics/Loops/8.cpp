/*
Escribir un programa que calcule el valor de:
1+3+5+...+2*(n)-1
*/

#include <iostream>
using namespace std;

int main()
{
    int n, suma = 0, i;

    cout << "Valor de n: ";
    cin >> n;

    for (i = 1; i <= 2 * n - 1; i += 2) {
        cout << i << ", ";
        suma += i;
    }

    cout << "\nSuma: " << suma << endl;

    return 0;
}

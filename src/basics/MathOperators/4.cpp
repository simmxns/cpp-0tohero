/*
3. Escribe la siguiente expresion matematica como exprecion en C++
a + (b/c) / d + (e/f)
*/

#include <iostream>
using namespace std;

int main()
{
    float a, b, c, d, e, f, resultado = 0;
    // hacer que el usuario digite el numero de los valores
    cout << "Valor de a: ";
    cin >> a;
    cout << "Valor de b: ";
    cin >> b;
    cout << "Valor de c: ";
    cin >> c;
    cout << "Valor de d: ";
    cin >> d;
    cout << "Valor de e: ";
    cin >> e;
    cout << "Valor de f: ";
    cin >> f;
    // operacion matematica
    resultado = (a + (b / c)) / (d + (e / f));
    // precisando resultado
    cout.precision(3);
    // mostrar resultado
    cout << "\nResultado: " << resultado << endl;

    return 0;
}

/*
1. Escribe la siguiente expresion como expresion en C++
a / b + 1
*/
#include <iostream>
using namespace std;

int main()
{
    // definiendo los valores a utilizar
    float a, b, resultado = 0;
    // haciendo que le usuario escriba los valores
    cout << "Digite el valor de a: ";
    cin >> a;
    cout << "Digite el valor de b: ";
    cin >> b;
    // operacion matematica
    resultado = (a / b) + 1;
    // cout.precision: Mostrar en pantalla la cantidad de numeros que desees
    cout.precision(3);
    // imprimir resultado
    cout << "\nEl resultado es: " << resultado << endl;

    return 0;
}

/* Escriba una funcion nombrada tiempo() que tenga un parametro en numero
entero llamada totalSeg y tres parametros de referencia enteros nombrados
horas, min y seg. La funcion debe convertir el numero de segundos transmitidos
en un numero equivalente de horas, minutos y segundos */

#include <iostream>
using namespace std;

void tiempo(int, int &, int &, int &);

int main()
{
    int totalSeg, horas, min, seg;

    cout << "Digite el numero total de segundos: ";
    cin >> totalSeg;

    tiempo(totalSeg, horas, min, seg);
    cout << "\nTiempo equivalente: " << endl;

    if (horas < 10)
        cout << 0;
    cout << horas;
    cout << ":";
    if (min < 10)
        cout << 0;
    cout << min;
    cout << ":";
    if (seg < 10)
        cout << 0;
    cout << seg;
    cout << endl;

    cin.ignore();
    cin.get();
    return 0;
}

void tiempo(int totalSeg, int &horas, int &min, int &seg)
{

    horas = totalSeg / 3600;
    totalSeg %= 3600;
    min = totalSeg / 60;
    seg = totalSeg % 60;
}
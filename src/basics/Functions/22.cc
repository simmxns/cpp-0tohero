/* Escriba una funcion en cpp llamada mayor() que devuelva la fecha mas reciente
de cualquier par de fechas que se le transmitan. Por ejemplo, si se transmiten
las fechas 10/09/2005 y 11/03/2015 a mayor(), sera devuelta la segunda fecha */

#include <iostream>
#include <stdlib.h>
using namespace std;

struct Date {
    int dia, mes, ano;
} f1, f2;

void llenarDatos();
Date fechaMayor(Date, Date);
void agCero(Date);
void mostrar(Date);

int main(int argc, char const *argv[])
{

    llenarDatos();
    Date f = fechaMayor(f1, f2);
    agCero(f);
    mostrar(f);

    cin.ignore();
    cin.get();
    system("pause");
    return 0;
}

void llenarDatos()
{
    cout << "Primer fecha\n";
    cout << "Dia: ";
    cin >> f1.dia;
    cout << "Mes: ";
    cin >> f1.mes;
    cout << "Año: ";
    cin >> f1.ano;
    cout << "\n";
    cout << "Segunda fecha\n";
    cout << "Dia: ";
    cin >> f2.dia;
    cout << "Mes: ";
    cin >> f2.mes;
    cout << "Año: ";
    cin >> f2.ano;
    cout << "\n";
}

Date fechaMayor(Date f1, Date f2)
{
    Date fechaMayor;

    if (f1.ano == f2.ano) {
        if (f1.mes == f2.mes) {
            if (f1.dia == f2.dia) {
                cout << "Ambas fechas son iguales";
            } else if (f1.dia > f2.dia) {
                fechaMayor = f1;
            } else {
                fechaMayor = f2;
            }
        } else if (f1.mes > f2.mes) {
            fechaMayor = f1;
        } else {
            fechaMayor = f2;
        }
    } else if (f1.ano > f2.ano) {
        fechaMayor = f1;
    } else {
        fechaMayor = f2;
    }

    return fechaMayor;
}

void agCero(Date f)
{
    if (f.dia < 10)
        cout << 0;
    if (f.mes < 10)
        cout << 0;
}

void mostrar(Date f)
{
    cout << "Fecha mayor: ";
    cout << f.dia << "/" << f.mes << "/" << f.ano << endl;
}
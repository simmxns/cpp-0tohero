/* Realice una funcion que tome como parametros un vector
de numeros enteros y devuelva la suma de sus elementos  */

#include <iostream>
using namespace std;

void pedirDatos();
int calcularSuma(int vec[], int);

int vec[100], tam;

int main()
{
    int res;
    pedirDatos();

    res = calcularSuma(vec, tam);
    cout << "\nSuma de los elementos del vector: " << res << endl;

    cin.ignore();
    cin.get();
    return 0;
}

void pedirDatos()
{
    cout << "Tamaño del vector: ";
    cin >> tam;

    for (int i = 0; i < tam; i++) {
        cout << i + 1 << ") Digite un numero: ";
        cin >> vec[i];
    }
}

int calcularSuma(int vec[], int tam)
{
    int suma = 0;

    for (int i = 0; i < tam; i++) {
        suma += vec[i];
    }

    return suma;
}
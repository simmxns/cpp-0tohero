/*
Escriba una plantilla de funcion llamda maximo() que devuelva el valor maximo
de tres argumentos que se transmitan a la funcion cuando sea llamda.
Suponga que los tres argumentos seran del mismo tipo de dato
*/

#include <iostream>

using namespace std;

template <class rand>
rand maximo(rand dato1, rand dato2, rand dato3);

char imprintToString(char data);

int main()
{
    double dato1 = 20.30, dato2 = 4.56, dato3 = -300.450;

    cout << "Valor de maximo valor: " << maximo(dato1, dato2, dato3);

    cin.ignore();
    cin.get();
    return 0;
}

template <class rand>
rand maximo(rand dato1, rand dato2, rand dato3)
{
    rand max;

    if ((dato1 > dato2) && (dato1 > dato3)) {
        max = dato1;
    } else if ((dato2 > dato1) && (dato2 > dato3)) {
        max = dato2;
    } else {
        max = dato3;
    }

    return max;
}
/*
Realizar un programa que lea un arreglo de estructuras los datos de N empleados
de la empresa y que imprima los datos del empleado con mayor y menor salario
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct Empleados {
    char nombre[20];
    float salario;
} empleado[100];

int main()
{
    int n;
    int menor, mayor = 0;
    int posmax = 0, posmin = 0;

    cout << "Digite la cantidad de empleados: ";
    cin >> n;
    cout << endl;

    for (int i = 0; i < n; i++) {
        cout << i + 1 << ") "
             << "Nombre: ";
        fflush(stdin);
        cin.getline(empleado[i].nombre, 20, '\n');
        cout << "Salario: ";
        fflush(stdin);
        cin >> empleado[i].salario;

        if (empleado[i].salario > mayor) { // Empleado con mayor salario
            mayor = empleado[i].salario;
            posmax = i;
        }
        menor = mayor;
        if (empleado[i].salario < menor) { // Empleado con menor salario
            menor = empleado[i].salario;
            posmin = i;
        }
    }

    // Imprimiendo a los empleados
    cout << "\n\nEmpleado con mayor salario";
    cout << "\nNombre: " << empleado[posmax].nombre;
    cout << "\nSalario: " << empleado[posmax].salario;

    cout << "\n\nEmpleado con menor salario";
    cout << "\nNombre: " << empleado[posmin].nombre;
    cout << "\nSalario: " << empleado[posmin].salario;
    cout << endl;
    system("pause");
    return 0;
}

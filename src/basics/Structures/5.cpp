/*
Hacer 2 estructuras una llamada promedio que tendra los siguientes campos:
nota1, nota2, nota3 y otra llamada alumno que tendra los siguientes campos:
nombre, sexo, edad; hacer que la estructura promedio este anidada en la
estructura alumno, luego pedir todos los datos para un alumno, luego calcular su
promedio, y por ultimo imprimir todos sus datos incluidos el promedio
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

struct promedio {
    float nota1;
    float nota2;
    float nota3;
};

struct alumno {
    char nombre[20];
    char sexo[20];
    int edad;
    struct promedio prom_alumno;
} alumnos[1];

int main()
{
    float prom;

    for (int i = 0; i < 1; i++) {
        cout << "Nombre: ";
        cin.getline(alumnos[i].nombre, 20, '\n');
        fflush(stdin);
        cout << "Sexo(M/F): ";
        cin.getline(alumnos[i].sexo, 20, '\n');
        fflush(stdin);
        cout << "Edad: ";
        cin >> alumnos[i].edad;
        fflush(stdin);
        printf("Notas del Alumno: \n");
        cout << "Nota 1: ";
        cin >> alumnos[i].prom_alumno.nota1;
        fflush(stdin);
        cout << "Nota 2: ";
        cin >> alumnos[i].prom_alumno.nota2;
        fflush(stdin);
        cout << "Nota 3: ";
        cin >> alumnos[i].prom_alumno.nota3;
        fflush(stdin);

        prom = (alumnos[i].prom_alumno.nota1 + alumnos[i].prom_alumno.nota2 +
                alumnos[i].prom_alumno.nota3);
    }

    for (int i = 0; i < 1; i++) {
        cout << "\nNombre: " << alumnos[i].nombre;
        cout << "\nSexo: " << alumnos[i].sexo;
        cout << "\nEdad: " << alumnos[i].edad;
        cout.precision(3);
        cout << "\nPromedio: " << prom / 3 << "\n\n";
    }

    system("pause");
    return 0;
}

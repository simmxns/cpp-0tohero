/*
Hacer un arreglo de estructura llamada atleta para N atletas
que contenga los siguientes campos: nombre, pais, numero_medallas.
Devolver los datos (nombre, pais) del atleta que ha ganado el mayor numero
medallas
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

struct atletas {
    char nombre[20];
    char pais[20];
    int numero_medallas[100];
} atletas[100];

int main()
{
    int n, pos = 0;
    int me_max = 0;
    cout << "Cantidad de atletas: ";
    cin >> n;

    for (int i = 0; i < n; i++) {
        cout << i + 1 << ") Nombre: ";
        fflush(stdin);
        cin.getline(atletas[i].nombre, 20, '\n');
        cout << "Pais: ";
        fflush(stdin);
        cin.getline(atletas[i].pais, 20, '\n');
        cout << "Cantidad de medallas: ";
        fflush(stdin);
        cin >> atletas[i].numero_medallas[i];

        if (atletas[i].numero_medallas[i] > me_max) {
            me_max = atletas[i].numero_medallas[i];
            pos = i;
        }
    }

    cout << "\nNombre: " << atletas[pos].nombre;
    cout << "\nPais: " << atletas[pos].pais;
    cout << endl;

    system("pause");
    return 0;
}

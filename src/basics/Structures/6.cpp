/*
Utilizar las 2 estructuras del problema 5, pero ahora pedir
los datos para N alumnos, y calcular cual de todos tiene le mejor
promedio, e imprimir sus datos
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

struct promedio {
    float nota1;
    float nota2;
    float nota3;
};

struct alumno {
    char nombre[20];
    char sexo[20];
    int edad;
    struct promedio prom_alumno;
} alumnos[100];

int main()
{
    float prom[100];
    int pos = 0, mayor = 0;
    int n;

    cout << "Cantidad de alumnos: ";
    cin >> n;

    for (int i = 0; i < n; i++) {
        cout << "\nDatos del alumno (" << i + 1 << ")\n";
        fflush(stdin);
        cout << "Nombre: ";
        cin.getline(alumnos[i].nombre, 20, '\n');
        fflush(stdin);
        cout << "Sexo(M/F): ";
        cin.getline(alumnos[i].sexo, 2, '\n');
        fflush(stdin);
        cout << "Edad: ";
        cin >> alumnos[i].edad;
        fflush(stdin);
        printf("|Notas del alumno|\n");
        fflush(stdin);
        // En una sola salida estandar se cargan las 3 notas
        cout << "Digite 3 notas: ";
        cin >> alumnos[i].prom_alumno.nota1 >> alumnos[i].prom_alumno.nota2 >>
            alumnos[i].prom_alumno.nota3;

        // Sumando las 3 notas de cada alumno
        prom[i] = (alumnos[i].prom_alumno.nota1 + alumnos[i].prom_alumno.nota2 +
                   alumnos[i].prom_alumno.nota3);
        // Encontrando al alumno de mayor promedio
        if (prom[i] > mayor) {
            mayor = prom[i];
            pos = i;
        }
    }

    printf("\n|Alumno con mayor promedio|");
    cout << "\nNombre: " << alumnos[pos].nombre;
    cout << "\nSexo: " << alumnos[pos].sexo;
    cout << "\nEdad: " << alumnos[pos].edad;
    cout.presicion(3);
    cout << "\nPromedio: " << prom[pos] / 3 << "\n\n";

    system("pause");
    return 0;
}

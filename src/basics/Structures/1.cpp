/*
Hacer una estructura llamada corredor, en la cual se tendran
los siguientes campos: nombre, edad, sexo, club pedir los datos al usuario
para un corredor, y asi una categoria de competicion:
- Juvenil <= 18 años
- Señor <= 40 años
- Veterano > 40 años
Posteriormente imprimir los datos del corredor, incluida su categoria de
competicion.
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct corredor {
    char nombre[30];
    int edad;
    char sexo[30];
    char club[30];
} corredores[3];

int main()
{
    char categoria[20];

    for (int i = 0; i < 3; i++) {
        cout << "Corredor [" << i + 1 << "]"
             << "\n";
        fflush(stdin);
        cout << "Nombre: ";
        fflush(stdin);
        cin.getline(corredores[i].nombre, 30, '\n');
        fflush(stdin);
        cout << "Edad: ";
        fflush(stdin);
        cin >> corredores[i].edad;
        fflush(stdin);
        cout << "Sexo(M/F): ";
        fflush(stdin);
        cin.getline(corredores[i].sexo, 30, '\n');
        fflush(stdin);
        cout << "Club: ";
        fflush(stdin);
        cin.getline(corredores[i].club, 30, '\n');
        fflush(stdin);
        cout << "\n";
    }

    // Imprimiento los campos de la estructura
    for (int i = 0; i < 3; i++) {
        // Imprimiento categoria del corredor
        if (corredores[i].edad <= 18) {
            strcpy(categoria, " Juvenil");
        } else if (corredores[i].edad <= 40) {
            strcpy(categoria, " Master");
        } else {
            strcpy(categoria, " Veterano");
        }
        // Imprimiendo campos del corredor
        cout << "\tCATEGORIA -" << categoria << endl;
        cout << "Nombre: " << corredores[i].nombre << endl;
        cout << "Edad: " << corredores[i].edad << endl;
        cout << "Sexo: " << corredores[i].sexo << endl;
        cout << "Club: " << corredores[i].club << endl;
        cout << endl;
    }

    system("pause");
    return 0;
}

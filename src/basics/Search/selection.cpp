/*
Ordenamiento por SELECCI�N

1. Buscar min elemento de la lista
2. Intercambiar con el 1� elemento
3. Buscar el min del resto de la lista
4. Intercambiar con el 2�
5. Se repite para el resto de numeros.
*/

#include <iostream>
#include <stdlib.h>

using namespace std;
int main()
{
    int num[] = {3, 2, 1, 5, 4};
    int i, j, aux, min;

    // Algoritmos de ordenamiento por seleccion
    for (i = 0; i < 5; i++) {
        min = i;
        for (j = i + 1; j < 5; j++) {
            if (num[j] < num[min]) {
                min = j;
            }
        }
        aux = num[i];
        num[i] = num[min];
        num[min] = aux;
    }

    cout << "Orden ASC: ";
    for (i = 0; i < 5; i++) {
        cout << num[i] << " ";
    }

    cout << "\nOrden DESC: ";
    for (i = 4; i > -1; i--) {
        cout << num[i] << " ";
    }
    cout << endl;

    system("pause");
    return 0;
}

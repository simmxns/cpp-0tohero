/*
Metodo BURBUJA

num actual > numero siguiente{
    cambio
  }
*/

#include <iostream>
#include <stdlib.h>

using namespace std;
int main()
{
    int num[] = {4, 1, 2, 3, 5};
    int i, j, aux;
    // Algoritmo del metodo burbuja
    for (i = 0; i < 5; i++) {
        for (j = 0; j < 5; j++) {
            if (num[j] >
                num[j + 1]) { // numero j es mayor a numero j siguiente 'j[j+1]'
                aux = num[j];
                num[j] = num[j + 1];
                num[j + 1] = aux;
            }
        }
    }

    cout << "Orden ASC: ";
    for (i = 0; i < 5; i++) {
        cout << num[i] << " ";
    }

    cout << "\nOrden DESC: ";
    for (i = 4; i > -1; i--) {
        cout << num[i] << " ";
    }
    cout << endl;

    system("pause");
    return 0;
}

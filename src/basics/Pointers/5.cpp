/** Info
 * @file ejercicio5.cc
 * @author sssimxn (offsivi@gmail.com)
 *
 * @details Rellenar un arreglo con n numeros, posteriormente utilizando
 *          punteros determinar el menor elemento del arreglo
 */

#include "conio.h"
#include "iostream"
using namespace std;

int pedirTAM();
void llenarArr(int[], int);
void arrBurbujeo(int[], int);

int main()
{
    int n, *dir_num, menor;
    int TAM = pedirTAM();
    int num[TAM];

    llenarArr(num, TAM);
    dir_num = num; // Obteniendo las posiciones de memoria
    arrBurbujeo(num, TAM);

    printf("\n");
    cout << "Menor valor: " << dir_num[0] << endl;
    cout << "Posicion de memoria: " << dir_num;

    getch();
    return 0;
}

int pedirTAM()
{
    int n;

    cout << "Digite el tam del arreglo: ";
    cin >> n;

    return n;
}

void llenarArr(int num[], int TAM)
{
    for (int i = 0; i < TAM; i++) {
        cout << "Digite un numero [" << i + 1 << "]: ";
        cin >> num[i];
    }
}

void arrBurbujeo(int num[], int TAM)
{
    int i, j, aux;

    for (i = 0; i < TAM; i++) {
        for (j = 0; j < TAM; j++) {
            if (num[j] > num[j + 1]) {
                aux = num[j];
                num[j] = num[j + 1];
                num[j + 1] = aux;
            }
        }
    }
}
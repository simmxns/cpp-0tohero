/*
Pedir al usuario que digite la cantidad de filas y columnas para una matriz
y llenarla de numeros aleatorios copiar esa matriz a otra y mostrar la matriz
copiada en pantalla
*/

#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int num1[100][100], f, c;
    int num2[100][100], dato = 0;

    cout << "Numero de filas: ";
    cin >> f;
    cout << "Numero de columnas: ";
    cin >> c;

    srand(time(NULL));
    // Recorriendo la primer matriz
    for (int i = 0; i < f; i++) {
        for (int j = 0; j < c; j++) {
            dato = 1 + rand() % (50); // Creando numeros aleatorios
            num1[i][j] =
                dato; // Y otorgandoselos a dichos numeros a la matriz "num1"
        }
    }
    // Igualando los valores de "num1" a "num2"
    for (int i = 0; i < f; i++) {
        for (int j = 0; j < c; j++) {
            num2[i][j] = num1[i][j];
        }
    }
    // Imprimiento la matriz "num2"
    for (int i = 0; i < f; i++) {
        for (int j = 0; j < c; j++) {
            cout << num2[i][j] << " ";
        }
        cout << "\n";
    }

    system("pause");
    return 0;
}

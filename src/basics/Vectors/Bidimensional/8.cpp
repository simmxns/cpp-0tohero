/*
Desarrollar un programa que me calcule el producto de 2 matrices
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int A[100][100], f1, c1;
    int B[100][100], f2, c2;
    int AB[100][100];

    cout << "Enter the rows quantity of your first matrix: ";
    cin >> f1;
    cout << "Enter the columns quantity of your first matrix: ";
    cin >> c1;
    cout << endl;

    cout << "Enter the rows quantity of your second matrix: ";
    cin >> f2;
    cout << "Enter the columns quantity of your second matrix: ";
    cin >> c2;
    cout << endl;

    if (c1 == f2) {
        // Primer matriz
        for (int i = 0; i < f1; i++) {
            for (int j = 0; j < c1; j++) {
                cout << "Enter the numbers of your first matrix[" << i << "]["
                     << j << "]: ";
                cin >> A[i][j];
            }
        }
        cout << endl;
        // Segunda matriz
        for (int i = 0; i < f2; i++) {
            for (int j = 0; j < c2; j++) {
                cout << "Enter the numbers of your second matrix[" << i << "]["
                     << j << "]: ";
                cin >> B[i][j];
            }
        }

        // Sacando el producto
        for (int i = 0; i < f1; i++) {
            for (int j = 0; j < c2; j++) {
                AB[i][j] = 0;
                for (int k = 0; k < c2; k++) {
                    AB[i][j] += A[i][k] * B[k][j];
                }
            }
        }

        // Imprimiendo el producto de las dos matrices
        for (int i = 0; i < f1; i++) {
            for (int j = 0; j < c2; j++) {
                cout << AB[i][j] << " ";
            }
            cout << "\n";
        }
    } else {
        cout << "ERROR";
    }

    system("pause");
    return 0;
}

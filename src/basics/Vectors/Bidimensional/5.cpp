/*
Crear una matriz y copiarla a otra de manera traspuesta.
Ejemplo:
|123|    |147|
|456| -> |258|
|789|    |369|
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
    int mat1[3][3];
    int mat2[3][3], n;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << "Digite un numero [" << i << "][" << j << "]: ";
            cin >> mat1[i][j];
        }
    }

    cout << "Matriz traspuesta";
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << mat1[j][i];
        }
        cout << "\n"
    }

    system("pause");
    return 0;
}

/*
Desarrolle un programa que lea de la entrada estandar
un vector de enteros y determine el mayor elemento del
vector
*/

#include <conio.h>
#include <iostream>
using namespace std;

int main()
{
    int n, mayor = 0;
    int numeros[n];

    cout << "Numero de posiciones de tu vector: ";
    cin >> n;

    for (int i = 0; i < n; i++) {
        cout << i + 1 << ") Numeros del vector: ";
        cin >> numeros[i];

        if (numeros[i] > mayor) {
            mayor = numeros[i];
        }
    }

    cout << "\nEl numero mayor de tu vector es: " << mayor << endl;

    getch();
    return 0;
}

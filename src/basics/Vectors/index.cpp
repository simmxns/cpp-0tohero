/*
-- SINTAXIS DE UN ARRAY --
int array[10]={1,2,3,4,5,6,7,8,9,10};

array = Nombre del array
[10] = Cantidad de posiciones de dicho array "AL PONER LOS DATOS EN TU ARRAY,
DICHO ARRAY SOBREENTIENDE LAS POSICIONES QUE CONTIENE" es decir este campo no es
tan necesario {1,2,3,4,5,6,7,8,9,10}; = Datos de tu array

Escribe un programa que defina un vector de numeros y calcule la suma de sus
elementos
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{
    int numeros[5] = {1, 2, 3, 4, 5};
    int suma = 0;

    for (int i = 0; i < 5; i++) {
        suma += numeros[i];
    }

    cout << "La suma del vector es: " << suma << endl;

    system("pause");
    return 0;
}

/*
Convertir dos cadenas de minus a MAYUS, comparar
dichas cadenas y decir si son iguales o no
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char cad1[100];
    char cad2[100];

    cout << "Digite una cadena en minus: ";
    gets(cad1);
    cout << "Digite otra cadena en minus: ";
    gets(cad2);

    strupr(cad1);
    strupr(cad2);

    if (strcmp(cad1, cad2) == 0) {
        cout << cad1 << " == " << cad2 << endl;
    } else {
        cout << cad1 << " != " << cad2 << endl;
    }

    system("pause");
    return 0;
}

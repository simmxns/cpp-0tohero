/*
HAcer un programa que pida al usuario digitar una cadena
de caracteres, pero si esta supera los 10 caracteres mostrarla
sino no mostrarla
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main()
{
    char cadena[100], len = 0;

    cout << "Digite una cadena de caracteres: ";
    gets(cadena);
    len = strlen(cadena);

    if (len > 10) {
        cout << cadena << endl;
    }

    system("pause");
    return 0;
}

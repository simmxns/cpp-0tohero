/*
Hacer un programa que al introducir una cadena se concatene con otra cadena
predefinida como "Hola que tal" de forma que quede la cadena introducida despues
de la predefinida Hola que tal <<cadena>>
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char cad1[] = "Hola que tal ";
    char nombre[100];

    cout << "Ingrese su nombre: ";
    gets(nombre);
    cout << endl;

    strcat(cad1, nombre);
    cout << cad1 << endl;

    system("pause");
    return 0;
}

/*
Cadena de caracteres
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main()
{
    char palabra[] = "Simon";
    char palabra2[] = {'S', 'i', 'm', 'o', 'n'};
    char nombre[20];

    cout << "Digite su nombre: ";
    cin.getline(nombre, 20, '\n');
    cout << nombre << endl;
    cout << palabra << endl;
    cout << palabra2 << endl;

    system("pause");
    return 0;
}

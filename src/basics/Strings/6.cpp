/*
Hacer un programa que determine si una palabra es
polindroma
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char cad1[100];
    char cad2[100];

    cout << "Ingrese una palabra: ";
    gets(cad1);

    strcpy(cad2, cad1);
    strrev(cad1);

    if (strcmp(cad1, cad2) == 0) {
        cout << "Polindroma" << endl;
    } else {
        cout << "No lo es" << endl;
    }

    system("pause");
    return 0;
}

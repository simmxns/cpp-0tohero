/*
Pasar una funcion a minusculas - Funcion strlwr()
*/

#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
int main()
{
    char cad[] = "SImON";

    strlwr(cad); // simon
    cout << cad << endl;

    system("pause");
    return 0;
}

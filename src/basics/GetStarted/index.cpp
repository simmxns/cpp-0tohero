// lectura y entrada de datos
#include <iostream>
#include <stdlib.h>
using namespace std;

int main()
{

    int numero;

    cout << "Digite un numero: ";
    cin >> numero;

    cout << "\nEl numero que digito es: " << numero;

    system("pause");
    return 0;
}

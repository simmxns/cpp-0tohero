#!/usr/local/bin/zsh

function clean {
  find . -type f ! -name "*.?*" -delete
  rm -rf ${PWD}/build/* ${PWD}/.cache/ ${PWD}/src/**/*.dSYM/ ${PWD}/*.DS_Store 
  echo "Cleaned!"
}

function format {
  clang-format -i ${PWD}/src/**/*.cpp
  echo "Formated!"
}

function build {
  cmake --build ${PWD}/build/
  echo "Build and ready to run!"
}

function run {
  ${PWD}/build/executable
}

start=`date +%s`

case "$1" in
"clean")
    clean
    ;;
"format")
    format
    ;;
"build")
    build
    ;;
"run")
    run
    ;;
*)
    "Invalid argument"
    ;;
esac

end=`date +%s`
RUNTIME=$( echo "$end - $start" | bc -l )

echo -e "\nDone in ${RUNTIME}s ✨"


